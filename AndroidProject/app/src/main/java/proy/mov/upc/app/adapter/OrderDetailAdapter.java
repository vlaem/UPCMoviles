package proy.mov.upc.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.List;

import proy.mov.upc.app.OrderActivity;
import proy.mov.upc.app.ProductOrderEditActivity;
import proy.mov.upc.app.R;
import proy.mov.upc.app.ss.dtos;

/**
 * Created by Alvaro on 17/02/2018.
 */

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderDetailHolder>{
    private List<Pair<dtos.Product, dtos.OrderDetail>> items;

    public static class OrderDetailHolder extends RecyclerView.ViewHolder{
        private int orderDetailId;
        private TextView lblProductName;
        private TextView lblUnitOfMeasurement;
        private TextView lblAmount;
        private TextView lblTotalPrice;
        private CardView orderdetail_card;

        public OrderDetailHolder(View v){
            super(v);
            this.orderdetail_card = (CardView) v.findViewById(R.id.orderdetail_card);
            this.lblProductName = (TextView) v.findViewById(R.id.lblOrderDetail_ProductName);
            this.lblUnitOfMeasurement = (TextView) v.findViewById(R.id.lblOrderDetail_unitOfMeasurement);
            this.lblAmount = (TextView) v.findViewById(R.id.lblOrderDetail_amount);
            this.lblTotalPrice = (TextView) v.findViewById(R.id.lblOrderDetail_totalPrice);

           orderdetail_card.setOnClickListener(new View.OnClickListener(){
               @Override
               public void onClick(View view) {
                   Intent newActivity = new Intent(view.getContext(), ProductOrderEditActivity.class);
                   newActivity.putExtra("OrderDetailId", orderDetailId );
                   view.getContext().startActivity(newActivity);
               }
           });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public OrderDetailAdapter(List<Pair<dtos.Product, dtos.OrderDetail>>  items){ this.items = items; }

    public List<Pair<dtos.Product, dtos.OrderDetail>> getItems() {return this.items;}

    @Override
    public OrderDetailAdapter.OrderDetailHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.orderdetail_card, viewGroup, false);
        return new OrderDetailAdapter.OrderDetailHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderDetailAdapter.OrderDetailHolder holder, int position) {
        Pair<dtos.Product,dtos.OrderDetail> item = items.get(position);
        holder.orderDetailId = item.second.getId();
        holder.lblProductName.setText(item.first.getName());
        holder.lblUnitOfMeasurement.setText(item.first.getUnitOfMeasurement());
        holder.lblAmount.setText(item.second.getAmount().toString());
        BigDecimal total = item.second.amount.multiply(item.first.getPrice());
        holder.lblTotalPrice.setText(total.toString());
    }
}
