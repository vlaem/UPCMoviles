package proy.mov.upc.app.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import proy.mov.upc.app.OrderActivity;
import proy.mov.upc.app.R;
import proy.mov.upc.app.ss.dtos;

/**
 * Created by Alvaro on 11/02/2018.
 */

public class DailyRouteAdapter extends RecyclerView.Adapter<DailyRouteAdapter.DailyRouteHolder> {
    private List<dtos.DailyRouteCustomer> items;

    public static class DailyRouteHolder extends RecyclerView.ViewHolder {
        private CardView dailyroute_card;
        private TextView lblCardCustomerName;
        private TextView lblCardCustomerAddress;
        private int dailyRouteId;

        public DailyRouteHolder(View v) {
            super(v);
            dailyroute_card = (CardView) v.findViewById(R.id.dailyroute_card);
            lblCardCustomerName = (TextView) v.findViewById(R.id.lblCardCustomerName);
            lblCardCustomerAddress = (TextView) v.findViewById(R.id.lblCardCustomerAddress);

            dailyroute_card.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent newActivity = new Intent(view.getContext(), OrderActivity.class);
                    newActivity.putExtra("DailyRouteId", dailyRouteId );
                    view.getContext().startActivity(newActivity);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public DailyRouteAdapter(List<dtos.DailyRouteCustomer> items){ this.items = items; }

    public List<dtos.DailyRouteCustomer> getItems() {return this.items;}

    @Override
    public DailyRouteHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.dailyroute_card, viewGroup, false);
        return new DailyRouteHolder(v);
    }

    @Override
    public void onBindViewHolder(DailyRouteHolder holder, int position) {
        holder.lblCardCustomerName.setText(items.get(position).getCustomerName());
        holder.lblCardCustomerAddress.setText(items.get(position).getCustomerAddress());
        holder.dailyRouteId = items.get(position).getId();
    }
}
