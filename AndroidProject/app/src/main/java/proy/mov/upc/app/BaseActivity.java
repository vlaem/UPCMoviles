package proy.mov.upc.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import proy.mov.upc.app.ss.ClientFactory;

public class BaseActivity extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setContentView(int layoutResID)
    {
        this.drawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        FrameLayout activityContainer = (FrameLayout) drawerLayout.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(drawerLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Activity Title");

        this.navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return NavSelected(item);
            }
        });
    }

    private boolean NavSelected(@NonNull MenuItem item){
        int id = item.getItemId();
        this.drawerLayout.closeDrawer(GravityCompat.START);
        if(id == R.id.action_routes){
            Intent newActivity = new Intent(this, DailyRouteActivity.class);
            this.startActivity(newActivity);
        }
        else if(id == R.id.action_sync){
            Intent newActivity = new Intent(this, SyncActivity.class);
            this.startActivity(newActivity);

        }
        else if(id == R.id.action_logout){
            SharedPreferences.Editor editor = getSharedPreferences("Qroma.App", MODE_PRIVATE).edit();
            editor.clear();
            editor.commit();
            ClientFactory.clearClient();
            Intent newActivity = new Intent(this, LoginActivity.class);
            newActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            this.startActivity(newActivity);
        }

        return true;
    }
}