package proy.mov.upc.app;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import net.servicestack.android.AndroidServiceClient;
import net.servicestack.client.AsyncResult;

import java.util.ArrayList;
import java.util.List;

import proy.mov.upc.app.database.CustomDbHelper;
import proy.mov.upc.app.ss.ClientFactory;
import proy.mov.upc.app.ss.dtos;

public class SyncActivity extends BaseActivity {

    private ProgressBar syncProgressBar;
    private Button btnSyncProducts;
    private Button btnSyncOrders;
    private Button btnSyncRoutes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        setTitle("Sync");

        this.btnSyncProducts = (Button) findViewById(R.id.btnSyncProducts);
        this.btnSyncOrders = (Button) findViewById(R.id.btnSyncOrders);
        this.btnSyncRoutes = (Button) findViewById(R.id.btnSyncRoutes);
        this.syncProgressBar = (ProgressBar) findViewById(R.id.syncProgressBar);

        this.syncProgressBar.setVisibility(View.INVISIBLE);

        btnSyncProducts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                ProductSync();
            }
        });
        btnSyncRoutes.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                RouteSync();
            }
        });
        btnSyncOrders.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                OrderSync();
            }
        });
    }

    private void OrderSync(){
        AndroidServiceClient client = ClientFactory.getClient();

        CustomDbHelper db = new CustomDbHelper(this);

        SharedPreferences prefs = getSharedPreferences("Qroma.App", MODE_PRIVATE);
        int userId = prefs.getInt("UserId", 0);

        ArrayList<dtos.OrderDetail> orders = db.GetOrderDetailsByUser(userId);

        dtos.ReplaceOrder request  = new dtos.ReplaceOrder();
        request.setOrderDetails(orders);

        syncProgressBar.setVisibility(View.VISIBLE);
        btnSyncProducts.setEnabled(false);
        btnSyncOrders.setEnabled(false);
        btnSyncRoutes.setEnabled(false);

        client.postAsync(request, new AsyncResult<ArrayList<String>>() {
                    @Override
                    public void success(ArrayList<String> response) {
                        super.success(response);
                        OnOrderSyncSuccess();
                    }

                    @Override
                    public void error(Exception ex) {
                        super.error(ex);
                        OnOrderSyncError(ex);
                    }
                }
        );
    }

    private void OnOrderSyncSuccess(){
        syncProgressBar.setVisibility(View.INVISIBLE);
        btnSyncProducts.setEnabled(true);
        btnSyncOrders.setEnabled(true);
        btnSyncRoutes.setEnabled(true);

        Toast toast = Toast.makeText(this, "Success!", Toast.LENGTH_LONG);
        toast.show();
    }

    private void OnOrderSyncError(Exception ex){
        String message;
        if (ex instanceof net.servicestack.client.WebServiceException) {
            net.servicestack.client.WebServiceException wsex = (net.servicestack.client.WebServiceException) ex;
            if(wsex.getResponseStatus() != null) {
                message = wsex.getResponseStatus().getMessage();
            }
            else{
                message = wsex.getErrorCode();
            }
        }
        else{
            message = "An error has occured";
        }

        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
        syncProgressBar.setVisibility(View.INVISIBLE);
        btnSyncProducts.setEnabled(true);
        btnSyncOrders.setEnabled(true);
        btnSyncRoutes.setEnabled(true);
    }

    private void RouteSync(){
        AndroidServiceClient client = ClientFactory.getClient();
        dtos.GetRouteList request = new dtos.GetRouteList();
        syncProgressBar.setVisibility(View.VISIBLE);
        btnSyncProducts.setEnabled(false);
        btnSyncOrders.setEnabled(false);
        btnSyncRoutes.setEnabled(false);

        client.getAsync(request, new AsyncResult<ArrayList<dtos.DailyRouteCustomer>>() {
            @Override
            public void success(ArrayList<dtos.DailyRouteCustomer> response) {
                super.success(response);
                OnRouteSyncSuccess(response);
            }

            @Override
            public void error(Exception ex) {
                super.error(ex);
                OnRouteSyncError(ex);
            }
        });
    }

    private void OnRouteSyncSuccess(ArrayList<dtos.DailyRouteCustomer> dailyRoutes){
        CustomDbHelper customDbHelper = new CustomDbHelper(this);
        customDbHelper.ReplaceDailyRoutes(dailyRoutes);

        syncProgressBar.setVisibility(View.INVISIBLE);
        btnSyncProducts.setEnabled(true);
        btnSyncOrders.setEnabled(true);
        btnSyncRoutes.setEnabled(true);

        Toast toast = Toast.makeText(this, "Success!", Toast.LENGTH_LONG);
        toast.show();
    }

    private void OnRouteSyncError(Exception ex){
        String message;
        if (ex instanceof net.servicestack.client.WebServiceException) {
            net.servicestack.client.WebServiceException wsex = (net.servicestack.client.WebServiceException) ex;
            if(wsex.getResponseStatus() != null) {
                message = wsex.getResponseStatus().getMessage();
            }
            else{
                message = wsex.getErrorCode();
            }
        }
        else{
            message = "An error has occured";
        }

        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
        syncProgressBar.setVisibility(View.INVISIBLE);
        btnSyncProducts.setEnabled(true);
        btnSyncOrders.setEnabled(true);
        btnSyncRoutes.setEnabled(true);
    }

    private void ProductSync(){
        AndroidServiceClient client = ClientFactory.getClient();
        dtos.GetProductList request = new dtos.GetProductList();
        syncProgressBar.setVisibility(View.VISIBLE);
        btnSyncProducts.setEnabled(false);
        btnSyncOrders.setEnabled(false);
        btnSyncRoutes.setEnabled(false);

        client.getAsync(request, new AsyncResult<ArrayList<dtos.Product>>() {
            @Override
            public void success(ArrayList<dtos.Product> response) {
                super.success(response);
                OnProductSyncSuccess(response);
            }

            @Override
            public void error(Exception ex) {
                super.error(ex);
                OnProductSyncError(ex);
            }
        });
    }

    private void OnProductSyncSuccess(ArrayList<dtos.Product> products){
        CustomDbHelper customDbHelper = new CustomDbHelper(this);
        customDbHelper.ReplaceProducts(products);
        Toast toast = Toast.makeText(this, "Success!", Toast.LENGTH_LONG);
        toast.show();
        syncProgressBar.setVisibility(View.INVISIBLE);
        btnSyncProducts.setEnabled(true);
        btnSyncOrders.setEnabled(true);
        btnSyncRoutes.setEnabled(true);
    }

    private void OnProductSyncError(Exception ex){
        String message;
        if (ex instanceof net.servicestack.client.WebServiceException) {
            net.servicestack.client.WebServiceException wsex = (net.servicestack.client.WebServiceException) ex;
            if(wsex.getResponseStatus() != null) {
                message = wsex.getResponseStatus().getMessage();
            }
            else{
                message = wsex.getErrorCode();
            }
        }
        else{
            message = "An error has occured";
        }

        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
        syncProgressBar.setVisibility(View.INVISIBLE);
        btnSyncProducts.setEnabled(true);
        btnSyncOrders.setEnabled(true);
        btnSyncRoutes.setEnabled(true);
    }

}
