/* Options:
Date: 2018-02-18 02:40:20
Version: 5.02
Tip: To override a DTO option, remove "//" prefix before updating
BaseUrl: https://localhost:44380

Package: proy.mov.upc.app.ss
GlobalNamespace: dtos
//AddPropertyAccessors: True
//SettersReturnThis: True
//AddServiceStackTypes: True
//AddResponseStatus: False
//AddDescriptionAsComments: True
//AddImplicitVersion: 
//IncludeTypes: 
//ExcludeTypes: 
//TreatTypesAsStrings: 
//DefaultImports: java.math.*,java.util.*,net.servicestack.client.*,com.google.gson.annotations.*,com.google.gson.reflect.*
*/

package proy.mov.upc.app.ss;

import java.math.*;
import java.util.*;
import net.servicestack.client.*;
import com.google.gson.annotations.*;
import com.google.gson.reflect.*;

public class dtos
{

    @Route("/app/{page}")
    public static class AppPageRequest implements IReturn<String>, IGet
    {
        public String page = null;
        
        public String getPage() { return page; }
        public AppPageRequest setPage(String value) { this.page = value; return this; }
        private static Object responseType = String.class;
        public Object getResponseType() { return responseType; }
    }

    @Route(Path="/Sync/Product", Verbs="GET")
    public static class GetProductList implements IReturn<ArrayList<Product>>, IGet
    {
        
        private static Object responseType = new TypeToken<ArrayList<Product>>(){}.getType();
        public Object getResponseType() { return responseType; }
    }

    @Route(Path="/Sync/Route", Verbs="GET")
    public static class GetRouteList implements IReturn<ArrayList<DailyRouteCustomer>>, IGet
    {
        
        private static Object responseType = new TypeToken<ArrayList<DailyRouteCustomer>>(){}.getType();
        public Object getResponseType() { return responseType; }
    }

    @Route(Path="/Sync/Order", Verbs="POST")
    public static class ReplaceOrder implements IReturn<ArrayList<String>>, IPost
    {
        public ArrayList<OrderDetail> orderDetails = null;
        
        public ArrayList<OrderDetail> getOrderDetails() { return orderDetails; }
        public ReplaceOrder setOrderDetails(ArrayList<OrderDetail> value) { this.orderDetails = value; return this; }
        private static Object responseType = new TypeToken<ArrayList<String>>(){}.getType();
        public Object getResponseType() { return responseType; }
    }

    @Route("/auth")
    // @Route("/auth/{provider}")
    // @Route("/authenticate")
    // @Route("/authenticate/{provider}")
    @DataContract
    public static class Authenticate implements IReturn<AuthenticateResponse>, IPost
    {
        @DataMember(Order=1)
        public String provider = null;

        @DataMember(Order=2)
        public String state = null;

        @DataMember(Order=3)
        public String oauth_token = null;

        @DataMember(Order=4)
        public String oauth_verifier = null;

        @DataMember(Order=5)
        public String userName = null;

        @DataMember(Order=6)
        public String password = null;

        @DataMember(Order=7)
        public Boolean rememberMe = null;

        @DataMember(Order=8)
        @SerializedName("continue") public String Continue = null;

        @DataMember(Order=9)
        public String nonce = null;

        @DataMember(Order=10)
        public String uri = null;

        @DataMember(Order=11)
        public String response = null;

        @DataMember(Order=12)
        public String qop = null;

        @DataMember(Order=13)
        public String nc = null;

        @DataMember(Order=14)
        public String cnonce = null;

        @DataMember(Order=15)
        public Boolean useTokenCookie = null;

        @DataMember(Order=16)
        public String accessToken = null;

        @DataMember(Order=17)
        public String accessTokenSecret = null;

        @DataMember(Order=18)
        public HashMap<String,String> meta = null;
        
        public String getProvider() { return provider; }
        public Authenticate setProvider(String value) { this.provider = value; return this; }
        public String getState() { return state; }
        public Authenticate setState(String value) { this.state = value; return this; }
        public String getOauthToken() { return oauth_token; }
        public Authenticate setOauthToken(String value) { this.oauth_token = value; return this; }
        public String getOauthVerifier() { return oauth_verifier; }
        public Authenticate setOauthVerifier(String value) { this.oauth_verifier = value; return this; }
        public String getUserName() { return userName; }
        public Authenticate setUserName(String value) { this.userName = value; return this; }
        public String getPassword() { return password; }
        public Authenticate setPassword(String value) { this.password = value; return this; }
        public Boolean isRememberMe() { return rememberMe; }
        public Authenticate setRememberMe(Boolean value) { this.rememberMe = value; return this; }
        public String getContinue() { return Continue; }
        public Authenticate setContinue(String value) { this.Continue = value; return this; }
        public String getNonce() { return nonce; }
        public Authenticate setNonce(String value) { this.nonce = value; return this; }
        public String getUri() { return uri; }
        public Authenticate setUri(String value) { this.uri = value; return this; }
        public String getResponse() { return response; }
        public Authenticate setResponse(String value) { this.response = value; return this; }
        public String getQop() { return qop; }
        public Authenticate setQop(String value) { this.qop = value; return this; }
        public String getNc() { return nc; }
        public Authenticate setNc(String value) { this.nc = value; return this; }
        public String getCnonce() { return cnonce; }
        public Authenticate setCnonce(String value) { this.cnonce = value; return this; }
        public Boolean isUseTokenCookie() { return useTokenCookie; }
        public Authenticate setUseTokenCookie(Boolean value) { this.useTokenCookie = value; return this; }
        public String getAccessToken() { return accessToken; }
        public Authenticate setAccessToken(String value) { this.accessToken = value; return this; }
        public String getAccessTokenSecret() { return accessTokenSecret; }
        public Authenticate setAccessTokenSecret(String value) { this.accessTokenSecret = value; return this; }
        public HashMap<String,String> getMeta() { return meta; }
        public Authenticate setMeta(HashMap<String,String> value) { this.meta = value; return this; }
        private static Object responseType = AuthenticateResponse.class;
        public Object getResponseType() { return responseType; }
    }

    @Route("/assignroles")
    @DataContract
    public static class AssignRoles implements IReturn<AssignRolesResponse>, IPost
    {
        @DataMember(Order=1)
        public String userName = null;

        @DataMember(Order=2)
        public ArrayList<String> permissions = null;

        @DataMember(Order=3)
        public ArrayList<String> roles = null;
        
        public String getUserName() { return userName; }
        public AssignRoles setUserName(String value) { this.userName = value; return this; }
        public ArrayList<String> getPermissions() { return permissions; }
        public AssignRoles setPermissions(ArrayList<String> value) { this.permissions = value; return this; }
        public ArrayList<String> getRoles() { return roles; }
        public AssignRoles setRoles(ArrayList<String> value) { this.roles = value; return this; }
        private static Object responseType = AssignRolesResponse.class;
        public Object getResponseType() { return responseType; }
    }

    @Route("/unassignroles")
    @DataContract
    public static class UnAssignRoles implements IReturn<UnAssignRolesResponse>, IPost
    {
        @DataMember(Order=1)
        public String userName = null;

        @DataMember(Order=2)
        public ArrayList<String> permissions = null;

        @DataMember(Order=3)
        public ArrayList<String> roles = null;
        
        public String getUserName() { return userName; }
        public UnAssignRoles setUserName(String value) { this.userName = value; return this; }
        public ArrayList<String> getPermissions() { return permissions; }
        public UnAssignRoles setPermissions(ArrayList<String> value) { this.permissions = value; return this; }
        public ArrayList<String> getRoles() { return roles; }
        public UnAssignRoles setRoles(ArrayList<String> value) { this.roles = value; return this; }
        private static Object responseType = UnAssignRolesResponse.class;
        public Object getResponseType() { return responseType; }
    }

    @Route("/register")
    @DataContract
    public static class Register implements IReturn<RegisterResponse>, IPost
    {
        @DataMember(Order=1)
        public String userName = null;

        @DataMember(Order=2)
        public String firstName = null;

        @DataMember(Order=3)
        public String lastName = null;

        @DataMember(Order=4)
        public String displayName = null;

        @DataMember(Order=5)
        public String email = null;

        @DataMember(Order=6)
        public String password = null;

        @DataMember(Order=7)
        public Boolean autoLogin = null;

        @DataMember(Order=8)
        @SerializedName("continue") public String Continue = null;
        
        public String getUserName() { return userName; }
        public Register setUserName(String value) { this.userName = value; return this; }
        public String getFirstName() { return firstName; }
        public Register setFirstName(String value) { this.firstName = value; return this; }
        public String getLastName() { return lastName; }
        public Register setLastName(String value) { this.lastName = value; return this; }
        public String getDisplayName() { return displayName; }
        public Register setDisplayName(String value) { this.displayName = value; return this; }
        public String getEmail() { return email; }
        public Register setEmail(String value) { this.email = value; return this; }
        public String getPassword() { return password; }
        public Register setPassword(String value) { this.password = value; return this; }
        public Boolean isAutoLogin() { return autoLogin; }
        public Register setAutoLogin(Boolean value) { this.autoLogin = value; return this; }
        public String getContinue() { return Continue; }
        public Register setContinue(String value) { this.Continue = value; return this; }
        private static Object responseType = RegisterResponse.class;
        public Object getResponseType() { return responseType; }
    }

    @Route("/session-to-token")
    @DataContract
    public static class ConvertSessionToToken implements IReturn<ConvertSessionToTokenResponse>, IPost
    {
        @DataMember(Order=1)
        public Boolean preserveSession = null;
        
        public Boolean isPreserveSession() { return preserveSession; }
        public ConvertSessionToToken setPreserveSession(Boolean value) { this.preserveSession = value; return this; }
        private static Object responseType = ConvertSessionToTokenResponse.class;
        public Object getResponseType() { return responseType; }
    }

    @Route("/access-token")
    @DataContract
    public static class GetAccessToken implements IReturn<GetAccessTokenResponse>, IPost
    {
        @DataMember(Order=1)
        public String refreshToken = null;
        
        public String getRefreshToken() { return refreshToken; }
        public GetAccessToken setRefreshToken(String value) { this.refreshToken = value; return this; }
        private static Object responseType = GetAccessTokenResponse.class;
        public Object getResponseType() { return responseType; }
    }

    @DataContract
    public static class AuthenticateResponse
    {
        @DataMember(Order=1)
        public String userId = null;

        @DataMember(Order=2)
        public String sessionId = null;

        @DataMember(Order=3)
        public String userName = null;

        @DataMember(Order=4)
        public String displayName = null;

        @DataMember(Order=5)
        public String referrerUrl = null;

        @DataMember(Order=6)
        public String bearerToken = null;

        @DataMember(Order=7)
        public String refreshToken = null;

        @DataMember(Order=8)
        public ResponseStatus responseStatus = null;

        @DataMember(Order=9)
        public HashMap<String,String> meta = null;
        
        public String getUserId() { return userId; }
        public AuthenticateResponse setUserId(String value) { this.userId = value; return this; }
        public String getSessionId() { return sessionId; }
        public AuthenticateResponse setSessionId(String value) { this.sessionId = value; return this; }
        public String getUserName() { return userName; }
        public AuthenticateResponse setUserName(String value) { this.userName = value; return this; }
        public String getDisplayName() { return displayName; }
        public AuthenticateResponse setDisplayName(String value) { this.displayName = value; return this; }
        public String getReferrerUrl() { return referrerUrl; }
        public AuthenticateResponse setReferrerUrl(String value) { this.referrerUrl = value; return this; }
        public String getBearerToken() { return bearerToken; }
        public AuthenticateResponse setBearerToken(String value) { this.bearerToken = value; return this; }
        public String getRefreshToken() { return refreshToken; }
        public AuthenticateResponse setRefreshToken(String value) { this.refreshToken = value; return this; }
        public ResponseStatus getResponseStatus() { return responseStatus; }
        public AuthenticateResponse setResponseStatus(ResponseStatus value) { this.responseStatus = value; return this; }
        public HashMap<String,String> getMeta() { return meta; }
        public AuthenticateResponse setMeta(HashMap<String,String> value) { this.meta = value; return this; }
    }

    @DataContract
    public static class AssignRolesResponse
    {
        @DataMember(Order=1)
        public ArrayList<String> allRoles = null;

        @DataMember(Order=2)
        public ArrayList<String> allPermissions = null;

        @DataMember(Order=3)
        public ResponseStatus responseStatus = null;
        
        public ArrayList<String> getAllRoles() { return allRoles; }
        public AssignRolesResponse setAllRoles(ArrayList<String> value) { this.allRoles = value; return this; }
        public ArrayList<String> getAllPermissions() { return allPermissions; }
        public AssignRolesResponse setAllPermissions(ArrayList<String> value) { this.allPermissions = value; return this; }
        public ResponseStatus getResponseStatus() { return responseStatus; }
        public AssignRolesResponse setResponseStatus(ResponseStatus value) { this.responseStatus = value; return this; }
    }

    @DataContract
    public static class UnAssignRolesResponse
    {
        @DataMember(Order=1)
        public ArrayList<String> allRoles = null;

        @DataMember(Order=2)
        public ArrayList<String> allPermissions = null;

        @DataMember(Order=3)
        public ResponseStatus responseStatus = null;
        
        public ArrayList<String> getAllRoles() { return allRoles; }
        public UnAssignRolesResponse setAllRoles(ArrayList<String> value) { this.allRoles = value; return this; }
        public ArrayList<String> getAllPermissions() { return allPermissions; }
        public UnAssignRolesResponse setAllPermissions(ArrayList<String> value) { this.allPermissions = value; return this; }
        public ResponseStatus getResponseStatus() { return responseStatus; }
        public UnAssignRolesResponse setResponseStatus(ResponseStatus value) { this.responseStatus = value; return this; }
    }

    @DataContract
    public static class RegisterResponse
    {
        @DataMember(Order=1)
        public String userId = null;

        @DataMember(Order=2)
        public String sessionId = null;

        @DataMember(Order=3)
        public String userName = null;

        @DataMember(Order=4)
        public String referrerUrl = null;

        @DataMember(Order=5)
        public String bearerToken = null;

        @DataMember(Order=6)
        public String refreshToken = null;

        @DataMember(Order=7)
        public ResponseStatus responseStatus = null;

        @DataMember(Order=8)
        public HashMap<String,String> meta = null;
        
        public String getUserId() { return userId; }
        public RegisterResponse setUserId(String value) { this.userId = value; return this; }
        public String getSessionId() { return sessionId; }
        public RegisterResponse setSessionId(String value) { this.sessionId = value; return this; }
        public String getUserName() { return userName; }
        public RegisterResponse setUserName(String value) { this.userName = value; return this; }
        public String getReferrerUrl() { return referrerUrl; }
        public RegisterResponse setReferrerUrl(String value) { this.referrerUrl = value; return this; }
        public String getBearerToken() { return bearerToken; }
        public RegisterResponse setBearerToken(String value) { this.bearerToken = value; return this; }
        public String getRefreshToken() { return refreshToken; }
        public RegisterResponse setRefreshToken(String value) { this.refreshToken = value; return this; }
        public ResponseStatus getResponseStatus() { return responseStatus; }
        public RegisterResponse setResponseStatus(ResponseStatus value) { this.responseStatus = value; return this; }
        public HashMap<String,String> getMeta() { return meta; }
        public RegisterResponse setMeta(HashMap<String,String> value) { this.meta = value; return this; }
    }

    @DataContract
    public static class ConvertSessionToTokenResponse
    {
        @DataMember(Order=1)
        public HashMap<String,String> meta = null;

        @DataMember(Order=2)
        public ResponseStatus responseStatus = null;
        
        public HashMap<String,String> getMeta() { return meta; }
        public ConvertSessionToTokenResponse setMeta(HashMap<String,String> value) { this.meta = value; return this; }
        public ResponseStatus getResponseStatus() { return responseStatus; }
        public ConvertSessionToTokenResponse setResponseStatus(ResponseStatus value) { this.responseStatus = value; return this; }
    }

    @DataContract
    public static class GetAccessTokenResponse
    {
        @DataMember(Order=1)
        public String accessToken = null;

        @DataMember(Order=2)
        public ResponseStatus responseStatus = null;
        
        public String getAccessToken() { return accessToken; }
        public GetAccessTokenResponse setAccessToken(String value) { this.accessToken = value; return this; }
        public ResponseStatus getResponseStatus() { return responseStatus; }
        public GetAccessTokenResponse setResponseStatus(ResponseStatus value) { this.responseStatus = value; return this; }
    }

    public static class Product
    {
        public Integer id = null;
        @StringLength(100)
        public String name = null;

        @StringLength(100)
        public String unitOfMeasurement = null;

        public BigDecimal price = null;
        
        public Integer getId() { return id; }
        public Product setId(Integer value) { this.id = value; return this; }
        public String getName() { return name; }
        public Product setName(String value) { this.name = value; return this; }
        public String getUnitOfMeasurement() { return unitOfMeasurement; }
        public Product setUnitOfMeasurement(String value) { this.unitOfMeasurement = value; return this; }
        public BigDecimal getPrice() { return price; }
        public Product setPrice(BigDecimal value) { this.price = value; return this; }
    }

    public static class DailyRouteCustomer
    {
        public Integer id = null;
        public Date date = null;
        public Integer customerId = null;
        public String customerRUC = null;
        public String customerName = null;
        public BigDecimal customerLatitude = null;
        public BigDecimal customerLongitude = null;
        public String customerAddress = null;
        public Integer userId = null;
        
        public Integer getId() { return id; }
        public DailyRouteCustomer setId(Integer value) { this.id = value; return this; }
        public Date getDate() { return date; }
        public DailyRouteCustomer setDate(Date value) { this.date = value; return this; }
        public Integer getCustomerId() { return customerId; }
        public DailyRouteCustomer setCustomerId(Integer value) { this.customerId = value; return this; }
        public String getCustomerRUC() { return customerRUC; }
        public DailyRouteCustomer setCustomerRUC(String value) { this.customerRUC = value; return this; }
        public String getCustomerName() { return customerName; }
        public DailyRouteCustomer setCustomerName(String value) { this.customerName = value; return this; }
        public BigDecimal getCustomerLatitude() { return customerLatitude; }
        public DailyRouteCustomer setCustomerLatitude(BigDecimal value) { this.customerLatitude = value; return this; }
        public BigDecimal getCustomerLongitude() { return customerLongitude; }
        public DailyRouteCustomer setCustomerLongitude(BigDecimal value) { this.customerLongitude = value; return this; }
        public String getCustomerAddress() { return customerAddress; }
        public DailyRouteCustomer setCustomerAddress(String value) { this.customerAddress = value; return this; }
        public Integer getUserId() { return userId; }
        public DailyRouteCustomer setUserId(Integer value) { this.userId = value; return this; }
    }

    public static class OrderDetail
    {
        public Integer id = null;
        public Integer dailyRouteId = null;
        public Integer productId = null;
        public BigDecimal amount = null;
        
        public Integer getId() { return id; }
        public OrderDetail setId(Integer value) { this.id = value; return this; }
        public Integer getDailyRouteId() { return dailyRouteId; }
        public OrderDetail setDailyRouteId(Integer value) { this.dailyRouteId = value; return this; }
        public Integer getProductId() { return productId; }
        public OrderDetail setProductId(Integer value) { this.productId = value; return this; }
        public BigDecimal getAmount() { return amount; }
        public OrderDetail setAmount(BigDecimal value) { this.amount = value; return this; }
    }

}
