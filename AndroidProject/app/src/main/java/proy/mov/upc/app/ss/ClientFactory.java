package proy.mov.upc.app.ss;

import net.servicestack.android.AndroidServiceClient;

/**
 * Created by Alvaro on 10/02/2018.
 */

public final class ClientFactory {
    private static AndroidServiceClient client = null;
    //private static final String baseUrl = "http://localhost:53909/";
    private static final String baseUrl = "https://upc-mov-web.azurewebsites.net";

    public static AndroidServiceClient getClient(){
        if(client == null){
            client = new AndroidServiceClient(baseUrl);
        }

        return client;
    }

    public static  AndroidServiceClient getClient(boolean newClient){
        if(client == null || newClient){
            client = new AndroidServiceClient(baseUrl);
        }

        return client;
    }

    public static void clearClient(){
        client = null;
    }
}
