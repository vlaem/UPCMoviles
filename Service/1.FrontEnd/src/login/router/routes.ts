import login from '../components/login/login.vue'
import { RouteConfig } from 'vue-router'

var routes: Array<RouteConfig> = [
    <RouteConfig>{
        path: '*',
        redirect: 'login'
    },
    <RouteConfig>{
        path: '/',
        name: 'login',
        component: login
    }
]

export { routes }