import Vue from 'vue'
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import layout from './components/_layout/layout.vue'
import { router }  from './router'

Vue.use(Vuetify);
Vue.use(VueRouter);

var vueInstance = new Vue({    
    router,    
    render: createElement => createElement(layout)
})

export { vueInstance }