import { orderDetailsComponents } from '../modules/orderdetails'
import { RouteConfig } from 'vue-router'

var orderRoutes: Array<RouteConfig> = [

    <RouteConfig>{
        path: '/order-details',
        name: 'order-details',
        component: orderDetailsComponents.orderdetails
    }
]

export { orderRoutes }