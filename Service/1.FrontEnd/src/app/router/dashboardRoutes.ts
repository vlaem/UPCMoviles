import dashboard from '../modules/dashboard/dashboard.vue'
import { RouteConfig } from 'vue-router'

var dashboardRoutes: Array<RouteConfig> = [
    <RouteConfig>{
        path: '*',
        redirect: 'dashboard'
    },
    <RouteConfig>{
        path: '/',
        name: 'dashboard',
        component: dashboard
    }
]

export { dashboardRoutes }