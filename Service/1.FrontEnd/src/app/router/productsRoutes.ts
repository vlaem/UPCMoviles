import { productsComponents } from '../modules/products'
import { RouteConfig } from 'vue-router'

var productsRoutes: Array<RouteConfig> = [
    <RouteConfig>{
        path: '/products',
        name:'products',
        component: productsComponents.product
    }
]

export { productsRoutes }
