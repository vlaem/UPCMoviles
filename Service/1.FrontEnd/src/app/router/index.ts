import VueRouter from "vue-router";
import { RouteConfig } from 'vue-router'
import { dashboardRoutes } from './dashboardRoutes'
import { productsRoutes } from './productsRoutes'
import { dailyRoutes } from './dailyRoutes'
import { orderRoutes } from './orderDetailsRoutes'

var routes: Array<RouteConfig> = [].concat(
    dashboardRoutes,
    productsRoutes,
    dailyRoutes,
    orderRoutes
)

var router = new VueRouter({
    routes
})

export { router }