import orderdetails from './orderdetails.vue'
import orderdetails_table from './orderdetails-table.vue'

const orderDetailsComponents = {
    orderdetails,
    orderdetails_table
}

export { orderDetailsComponents }