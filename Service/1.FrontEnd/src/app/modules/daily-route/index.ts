import daily_route from './daily-route.vue'
import daily_route_table from './daily-route-table.vue'

const dailyRouteComponents = {
    daily_route,
    daily_route_table
}

export { dailyRouteComponents }