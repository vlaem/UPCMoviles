import product_table from './product-table.vue'
import product from './product.vue'

const productsComponents = {
    product_table,
    product
}

export { productsComponents }