import { layoutComponents } from './_layout'
import { dashboardComponents } from './dashboard'
import { productsComponents } from './products'
import { dailyRouteComponents } from './daily-route'
import { orderDetailsComponents } from './orderdetails'

const components = Object.assign(
    {},
    layoutComponents,
    dashboardComponents,
    productsComponents,
    dailyRouteComponents,
    orderDetailsComponents
)

export { components }