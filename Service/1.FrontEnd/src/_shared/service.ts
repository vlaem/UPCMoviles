import { JsonServiceClient } from 'servicestack-client';
import * as dtos from './types'

class ServiceFactory {
    static createClient(authRequired: Boolean = true): JsonServiceClient {
        var serviceClient: JsonServiceClient = new JsonServiceClient('/')
        if (authRequired) {
            serviceClient.onAuthenticationRequired = async () => {
                window.location.replace("/login/");
            };
        }

        return serviceClient
    }
}

export { ServiceFactory, dtos }