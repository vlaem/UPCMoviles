const gulp = require('gulp')
const request = require('request');
const fs = require('fs')

gulp.task('ss-update-types', function () {
    return request('http://localhost:53909/types/typescript')
        .pipe(fs.createWriteStream('./src/_shared/types.ts'));    
})

gulp.task('ss-update-types-d', function () {
    return request('http://localhost:53909/types/typescript.d')
        .pipe(fs.createWriteStream('./src/_shared/types.d.ts'));
})

var tasks = ['ss-update-types','ss-update-types-d']

gulp.task('ss-update', tasks);