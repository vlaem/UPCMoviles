const baseWebpackConfig = require('./webpack.base.conf')
const config = require('../config')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
const merge = require('webpack-merge')
const webpack = require('webpack')

module.exports = merge(baseWebpackConfig, {
  // cheap-module-eval-source-map is faster for development
  devtool: '#cheap-module-eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      ENV: 'local'
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new BrowserSyncPlugin(config.local.browserSyncOptions),
    new HtmlWebpackPlugin({
      chunks: ['login'],
      template: './src/login/login.html',
      filename: '../login.html'
    }),
    new HtmlWebpackPlugin({
      chunks: ['app'],
      template: './src/app/app.html',
      filename: '../app.html'
    })
  ]
})

module.exports.watch = true