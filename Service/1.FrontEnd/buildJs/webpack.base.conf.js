'use strict'
const path = require('path')
const config = require('../config')

function resolve(dir) {
    return path.join(__dirname, '..', dir)
}

module.exports = {
    entry: {
        'login': './src/login/',
        'app': './src/app/',
    },
    output: {
        path: config.build.jsRoot,
        publicPath: '/js/',
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                include: resolve('src'),
                options: {
                    loaders: {
                        js: 'ts-loader'
                    }
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: resolve('src'),
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                },
                include: resolve('src'),
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
        ]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.vue'],
        modules: [resolve('src'), 'node_modules'],
        alias: {
            "shared": path.resolve(__dirname, '../src/_shared/'),
        }
    },
}