﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UPC.Mov.Core.Mapping.Views
{
    public class DailyRouteCustomer
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public string CustomerRUC { get; set; }
        public string CustomerName { get; set; }
        public decimal CustomerLatitude { get; set; }
        public decimal CustomerLongitude { get; set; }
        public string CustomerAddress { get; set; }
        public int UserId { get; set; }
    }
}
