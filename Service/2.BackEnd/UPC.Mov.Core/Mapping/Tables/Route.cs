﻿using ServiceStack.DataAnnotations;
using System;

namespace UPC.Mov.Core.Mapping.Tables
{
    public class DailyRoute
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public int UserId { get; set; }

        public int CustomerId { get; set; }
    }
}