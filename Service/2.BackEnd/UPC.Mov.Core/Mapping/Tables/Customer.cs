﻿using ServiceStack.DataAnnotations;

namespace UPC.Mov.Core.Mapping.Tables
{
    public class Customer
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }

        [StringLength(100)]
        public string RUC { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}