﻿using ServiceStack;

namespace UPC.Mov.Core.Dtos
{
    [Route("/app/{page}")]
    public class AppPageRequest : IGet, IReturn<string>
    {
        public string Page { get; set; }
    }    
}