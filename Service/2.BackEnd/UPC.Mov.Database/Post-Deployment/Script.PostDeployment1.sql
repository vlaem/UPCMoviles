﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.UserAuth)
BEGIN
	SET IDENTITY_INSERT [dbo].[UserAuth] ON 
	INSERT dbo.UserAuth (Id, UserName, Email, FirstName, LastName, DisplayName, FullName, PasswordHash, Roles, Permissions, CreatedDate, ModifiedDate, InvalidLoginAttempts) 
	VALUES 
	(1, N'admin', N'admin@qroma.com', N'Super', N'Admin', N'Super Admin', N'Super Admin', N'AQAAAAEAACcQAAAAEGpu3CYPWSLE6YDQqe/whcbC/thu89GNYg10aze1A0VXbJc4qikw5X8k9ydywQB1kQ==', N'["Admin"]', N'[]', CAST(N'2018-01-22T15:05:19.710' AS DateTime), CAST(N'2018-01-22T15:05:19.840' AS DateTime), 0),
	(2, N'User1', N'user1@qroma.com', N'User', N'1', N'Super Admin', N'Super Admin', N'AQAAAAEAACcQAAAAEGpu3CYPWSLE6YDQqe/whcbC/thu89GNYg10aze1A0VXbJc4qikw5X8k9ydywQB1kQ==', N'["Admin"]', N'[]', CAST(N'2018-01-22T15:05:19.710' AS DateTime), CAST(N'2018-01-22T15:05:19.840' AS DateTime), 0),
	(3, N'User2', N'user2@qroma.com', N'User', N'2', N'Super Admin', N'Super Admin', N'AQAAAAEAACcQAAAAEGpu3CYPWSLE6YDQqe/whcbC/thu89GNYg10aze1A0VXbJc4qikw5X8k9ydywQB1kQ==', N'["Admin"]', N'[]', CAST(N'2018-01-22T15:05:19.710' AS DateTime), CAST(N'2018-01-22T15:05:19.840' AS DateTime), 0)
	SET IDENTITY_INSERT [dbo].[UserAuth] OFF
END

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.Product)
BEGIN
	INSERT INTO Product(Name,UnitOfMeasurement,Price)
	VALUES
	('DESMOLTEK FG BALDE PLASTICO 3KG', 'UNI', 4.0000),
	('TEROKAL ROYAL  3.785 LT', 'UNI', 4.0000),
	('SMALTE SINTETICO PINTOR VERDE CROMO 1 GLS', 'UNI', 20.0000),
	('ESMALTE PATO AZUL ELECTRICO 1 GL', 'UNI', 40.0000),
	('LATEX TEKNOLATEX BLANCO 20 LT', 'UNI', 8.0000),
	('FASTIPOXI ESMALTE EPOXICO - PARTE B . 1/2 GL', 'UNI', 40.000 ),
	('AMERICAN COLORS GRIS CALIDO 4 LT', 'UNI', 12.000 )
END

IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.Customer)
BEGIN
	INSERT INTO Customer(Name, RUC, Longitude, Latitude, Address)
	VALUES
	('INDUSTRIAS Q 2000 S.A.C','20121245783',-77.046593,-12.065803, 'Av. Brasil 680, Breña'),
	('FERRETERIA FERREK','20124589783',-77.04154,-12.07256, 'Av. Salaverry 740, Jesús María'),
	('PROMART','20785265413',-77.041883,-12.076421,'Av. Francisco Javier Mariátegui 674, Jesús María')
END


IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.DailyRoute)
BEGIN
	INSERT INTO DailyRoute(Date, CustomerId, UserId)
	VALUES
	('20180202',1,2),
	('20180202',2,2),
	('20180202',3,2),
	('20180202',1,3),
	('20180202',2,3),
	('20180202',3,3)
END