﻿CREATE TABLE [dbo].[UserAuthRole] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [UserAuthId]   INT            NOT NULL,
    [Role]         VARCHAR (8000) NULL,
    [Permission]   VARCHAR (8000) NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [ModifiedDate] DATETIME       NOT NULL,
    [RefId]        INT            NULL,
    [RefIdStr]     VARCHAR (8000) NULL,
    [Meta]         VARCHAR (MAX)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

