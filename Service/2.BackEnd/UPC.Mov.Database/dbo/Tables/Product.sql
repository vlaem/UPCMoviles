﻿CREATE TABLE [dbo].[Product] (
    [Id]       INT             IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (100)  NULL,
    [UnitOfMeasurement] VARCHAR (100)  NULL,
    [Price]    DECIMAL (18, 6) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

