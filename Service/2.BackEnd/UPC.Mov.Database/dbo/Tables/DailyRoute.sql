﻿CREATE TABLE [dbo].[DailyRoute] (
    [Id]         INT      IDENTITY (1, 1) NOT NULL,
    [Date]       DATETIME NOT NULL,
    [CustomerId] INT      NOT NULL,
    [UserId] INT NOT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

