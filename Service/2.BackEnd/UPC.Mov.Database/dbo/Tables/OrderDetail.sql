﻿CREATE TABLE [dbo].[OrderDetail]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DailyRouteId] INT NOT NULL, 
    [ProductId] INT NOT NULL, 
    [Amount] DECIMAL(18, 3) NOT NULL
)
