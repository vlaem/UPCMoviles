﻿CREATE TABLE [dbo].[Customer] (
    [Id]        INT             IDENTITY (1, 1) NOT NULL,
    [RUC]       VARCHAR (100)  NULL,
    [Name]      VARCHAR (100)  NULL,
    [Latitude]  DECIMAL (9, 6) NOT NULL,
    [Longitude] DECIMAL (9, 6) NOT NULL,
    [Address] VARCHAR(300) NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

