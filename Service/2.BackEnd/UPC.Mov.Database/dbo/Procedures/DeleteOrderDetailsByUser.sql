﻿CREATE PROCEDURE [dbo].[DeleteOrderDetailsByUser]
(
	@UserId int
)
AS
BEGIN
	DELETE OD
	FROM OrderDetail OD
	INNER JOIN DailyRoute DR
	  ON OD.DailyRouteId = DR.Id
	WHERE DR.UserId = @UserId
END

