﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using ServiceStack.Configuration;
using System.IO;
using ServiceStack;

namespace UPC.Mov.WebSite
{
    public class Startup
    {
        private MultiAppSettings Settings { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                                .SetBasePath(env.ContentRootPath)
                                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                                .AddEnvironmentVariables();

            var settings = new List<IAppSettings>();

            if (File.Exists($"Settings/app.settings.txt".MapProjectPlatformPath()))
            {
                settings.Add(new TextFileSettings($"Settings/app.settings.txt".MapProjectPlatformPath()));
            }

            if (File.Exists($"Settings/app.settings.{env.EnvironmentName}.txt".MapProjectPlatformPath()))
            {
                settings.Add(new TextFileSettings($"Settings/app.settings.{env.EnvironmentName}.txt".MapProjectPlatformPath()));
            }

            this.Settings = new MultiAppSettings(settings.ToArray());            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var appHost = new AppHost(this.Settings);
            app.UseServiceStack(appHost);
        }
    }
}