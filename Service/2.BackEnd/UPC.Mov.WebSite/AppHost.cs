﻿using ServiceStack;
using Funq;
using UPC.Mov.ApplicationServices;
using ServiceStack.Configuration;
using ServiceStack.OrmLite;
using ServiceStack.Data;
using ServiceStack.Auth;


namespace UPC.Mov.WebSite
{
    public class AppHost : AppHostBase
    {
        private IAppSettings _tempAppSettings;

        public AppHost(IAppSettings settings) : base("MyAppHost", typeof(ProductService).Assembly)
        {
            this._tempAppSettings = settings;
        }

        public override void Configure(Container container)
        {
            this.AppSettingsConfig(container);
            this.AuthFeature(container);
            this.RegisterDependencies(container);
            this.DataBaseInit(container);

            SetConfig(new HostConfig
            {
                DefaultRedirectPath = "/app/login/",
                UseHttpsLinks = true
            });
        }

        private void AppSettingsConfig(Container container)
        {
            container.Register<IDbConnectionFactory>(
                new OrmLiteConnectionFactory(
                        this._tempAppSettings.Get<string>("Mov"),
                        SqlServer2016Dialect.Provider
                    )
             );

            using(var context = container.Resolve<IDbConnectionFactory>().OpenDbConnection())
            {
                context.CreateTableIfNotExists<Core.Mapping.Tables.Customer>();
                context.CreateTableIfNotExists<Core.Mapping.Tables.Product>();
                context.CreateTableIfNotExists<Core.Mapping.Tables.DailyRoute>();
            }

            container.Register(c => new OrmLiteAppSettings(container.Resolve<IDbConnectionFactory>()));

            var ormListAppSettings = container.Resolve<OrmLiteAppSettings>();
            ormListAppSettings.InitSchema();

            this.AppSettings = new MultiAppSettings(new IAppSettings[] { this._tempAppSettings, ormListAppSettings });
        }

        private void AuthFeature(Container container)
        {
            Plugins.Add(new AuthFeature(() => new AuthUserSession(),
                                new IAuthProvider[] {
                    new JwtAuthProvider()
                    {
                        HashAlgorithm = AppSettings.Get<string>("HashAlgorithm"),
                        PublicKeyXml = AppSettings.Get<string>("PublicKey"),
                        PrivateKeyXml = AppSettings.Get<string>("PrivateKey"),
                        Audience = "UPC.QROMA",
                        RequireSecureConnection = false,
                    },
                    new CredentialsAuthProvider(),
                    })
            {
                IncludeRegistrationService = true,
                ValidateUniqueEmails = true,
                ValidateUniqueUserNames = true,
            });
        }

        private void RegisterDependencies(Container container)
        {
            container.Register<IUserAuthRepository>(c => new OrmLiteAuthRepository<UserAuth, UserAuthDetails>(c.Resolve<IDbConnectionFactory>()));
        }

        private void DataBaseInit(Container container)
        {
            var authRepo = container.Resolve<IUserAuthRepository>();
            authRepo.InitSchema();
        }

        private void Filters(Container container)
        {
        }
    }
}
