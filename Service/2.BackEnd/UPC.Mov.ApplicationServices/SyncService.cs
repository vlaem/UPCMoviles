﻿using System.Collections.Generic;
using System.Linq;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using UPC.Mov.Core.Dtos;
using UPC.Mov.Core.Mapping.Tables;
using UPC.Mov.Core.Mapping.Views;

namespace UPC.Mov.ApplicationServices
{
    public class ProductService : Service
    {
        private IDbConnectionFactory _dbFactory;

        public ProductService(IDbConnectionFactory dbFactory)
        {
            this._dbFactory = dbFactory;
        }

        [Authenticate]
        public IEnumerable<Product> Get(GetProductList request)
        {
            using (var context = _dbFactory.OpenDbConnection())
            {
                var query = context.From<Product>();
                List<Product> result = context.Select(query);
                return result;
            }
        }

        [Authenticate]
        public IEnumerable<DailyRouteCustomer> Get(GetRouteList request)
        {
            using (var context = _dbFactory.OpenDbConnection())
            {
                var session = this.GetSession();
                var userId = int.Parse(session.UserAuthId);

                var query = context.From<DailyRouteCustomer>()
                            .Where(x => x.UserId == userId);

                var result = context.Select(query);

                return result;
            }
        }

        [Authenticate]
        public IEnumerable<string> Post(ReplaceOrder request)
        {
            using (var context = _dbFactory.OpenDbConnection())
            {
                var session = this.GetSession();
                var userId = int.Parse(session.UserAuthId);

                context.ExecuteSql("exec DeleteOrderDetailsByUser @UserId", new { UserId = userId });

                foreach (var od in request.OrderDetails)
                {
                    context.Save(new OrderDetail()
                    {
                        Amount = od.Amount,
                        DailyRouteId = od.DailyRouteId,
                        ProductId = od.ProductId
                    });
                }

                return new List<string>();
            }
        }

        public IEnumerable<OrderDetail> Get(GetOrderDetails request)
        {
            using (var context = _dbFactory.OpenDbConnection())
            {
                var session = this.GetSession();
                var userId = int.Parse(session.UserAuthId);

                var query = context.From<OrderDetail>()
                                    .Join<DailyRoute>((x, y) => x.DailyRouteId == y.Id)
                                    .Where<DailyRoute>(x => x.UserId == userId);

                return context.Select(query);
            }
        }
    }
}